package com.unaimasa.baseproject.rest.entities;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by UnaiMasa on 22/05/2016.
 */
@RunWith(RobolectricTestRunner.class)
public class InfoTest {

    private Info mInfo;

    @Before
    public void setUp() {
        mInfo = new Info();
    }

    @Test
    public void rel_isCorrect() throws Exception {
        String value = "rel";
        mInfo.setRel(value);
        assertEquals(mInfo.getRel(), value);
    }

    @Test
    public void title_isCorrect() throws Exception {
        String value = "title";
        mInfo.setTitle(value);
        assertEquals(mInfo.getTitle(), value);
    }

}
