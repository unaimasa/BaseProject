package com.unaimasa.baseproject.rest.entities;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by UnaiMasa on 22/05/2016.
 */
@RunWith(RobolectricTestRunner.class)
public class MovieTest {

    private Movie mMovie;

    @Before
    public void setUp() {
        mMovie = new Movie();
    }

    @Test
    public void id_isCorrect() throws Exception {
        String value = "id";
        mMovie.setId(value);
        assertEquals(mMovie.getId(), value);
    }

    @Test
    public void title_isCorrect() throws Exception {
        String value = "title";
        mMovie.setTitle(value);
        assertEquals(mMovie.getTitle(), value);
    }

    @Test
    public void shortDescription_isCorrect() throws Exception {
        String value = "shortDescription";
        mMovie.setShortDescription(value);
        assertEquals(mMovie.getShortDescription(), value);
    }

    @Test
    public void synopsis_isCorrect() throws Exception {
        String value = "synopsis";
        mMovie.setSynopsis(value);
        assertEquals(mMovie.getSynopsis(), value);
    }

    @Test
    public void broadcastChannelValue_isCorrect() throws Exception {
        String value = "broadcastChannelValue";
        mMovie.setBroadcastChannelValue(value);
        assertEquals(mMovie.getBroadcastChannelValue(), value);
    }

    @Test
    public void href_isCorrect() throws Exception {
        String value = "href";
        mMovie.set_Href(value);
        assertEquals(mMovie.get_Href(), value);
    }

}
