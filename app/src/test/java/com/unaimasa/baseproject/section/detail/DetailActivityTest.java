package com.unaimasa.baseproject.section.detail;


import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;

import com.unaimasa.baseproject.BuildConfig;
import com.unaimasa.baseproject.Constants;
import com.unaimasa.baseproject.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;


/**
 * Created by UnaiMasa on 22/05/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class DetailActivityTest {

    DetailActivity activity;

    // Create DetailActivity
    @Before
    public void setup() {
        // Get Application Context
        final ShadowApplication shadowApplication = Shadows.shadowOf(RuntimeEnvironment.application);

        //Create an Intent With Extras
        Intent intent = new Intent(shadowApplication.getApplicationContext(), DetailActivity.class);
        intent.putExtra(Constants.IntentExtras.MOVIE_HREF, "http://content.ott.sky.com/v2/brands/go/devices/ios/content/item-summaries/5beb793d435a4510VgnVCM1000000b43150a____");
        // Create the Activity
        activity = Robolectric.buildActivity(DetailActivity.class).withIntent(intent).create().get();
    }

    @Test
    public void check_notNull() {
        assertThat(activity,is(notNullValue()));

        ImageView ivMovie = (ImageView) activity.findViewById(R.id.iv_movie);
        assertThat(ivMovie, is(notNullValue()));

        TextView tvTitle = (TextView) activity.findViewById(R.id.tv_movie_title);
        assertThat(tvTitle, is(notNullValue()));

        TextView tvBroadcastChannel = (TextView) activity.findViewById(R.id.tv_movie_broadcast_channel);
        assertThat(tvBroadcastChannel, is(notNullValue()));

        TextView tvSynopsis = (TextView) activity.findViewById(R.id.tv_movie_synopsis);
        assertThat(tvSynopsis, is(notNullValue()));
    }

}
