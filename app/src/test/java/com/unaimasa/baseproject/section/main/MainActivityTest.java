package com.unaimasa.baseproject.section.main;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.unaimasa.baseproject.BuildConfig;
import com.unaimasa.baseproject.Constants;
import com.unaimasa.baseproject.R;
import com.unaimasa.baseproject.rest.entities.Movie;
import com.unaimasa.baseproject.section.detail.DetailActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by UnaiMasa on 22/05/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MainActivityTest {

    MainActivity activity;

    // Create DetailActivity
    @Before
    public void setup() {
        // Create the Activity
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
    }

    @Test
    public void check_notNull() {
        assertThat(activity, is(notNullValue()));

        RecyclerView myRecyclerView = (RecyclerView) activity.findViewById(R.id.my_recycler_view);
        assertThat(myRecyclerView, is(notNullValue()));
    }

    @Test
    public void check_ShowMovieInformation() {
        // Get Application Context
        final ShadowApplication shadowApplication = Shadows.shadowOf(RuntimeEnvironment.application);

        Movie movie = new Movie("id", "title", "shortDescription", "synopsis",
                "broadcastChannelValue", "href");

        activity.showMovieInformation(movie);

        Intent expectedIntent = new Intent(activity, DetailActivity.class);
        expectedIntent.putExtra(Constants.IntentExtras.MOVIE_HREF, movie.get_Href());
        assertThat(shadowApplication.getNextStartedActivity(), is(expectedIntent));
    }

}
