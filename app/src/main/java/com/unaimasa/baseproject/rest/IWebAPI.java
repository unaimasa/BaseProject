package com.unaimasa.baseproject.rest;

import com.unaimasa.baseproject.rest.entities.Info;
import com.unaimasa.baseproject.rest.entities.Movie;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by UnaiMasa on 21/05/2016.
 */
public interface IWebAPI {

    @GET("/v2/brands/go/devices/ios/navigation/nodes/177da30fc6850410VgnVCM100000625112ac____/pages/7908e6066a051410VgnVCM1000000b43150a____?represent=(item-group/item-group(item/item-summary(broadcast-channel/broadcast-channel)))")
    Call<Info> getMovieList();

    @GET("{path}")
    Call<Movie> getMovie(@Path(value = "path", encoded = true) String path);

}
