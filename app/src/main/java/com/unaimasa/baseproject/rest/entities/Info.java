package com.unaimasa.baseproject.rest.entities;

import java.util.ArrayList;

/**
 * Created by UnaiMasa on 21/05/2016.
 */
public class Info {
    private String rel, title;
    private ArrayList<MovieList> _links;

    public Info() {
    }

    public Info(String rel, String title, ArrayList<MovieList> _links) {
        this.rel = rel;
        this.title = title;
        this._links = _links;
    }

    public String getRel() { return rel; }

    public void setRel(String rel) { this.rel = rel; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public ArrayList<MovieList> get_Links() { return _links; }

    public void set_Links(ArrayList<MovieList> _links) { this._links = _links; }

}
