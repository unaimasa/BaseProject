package com.unaimasa.baseproject.rest.entities;

import java.util.List;

/**
 * Created by UnaiMasa on 21/05/2016.
 */
public class Movie {
    private String id, title, shortDescription, synopsis, broadcastChannelValue, _href;

    private List<Image> images;

    public Movie() {
    }

    public Movie(String id, String title, String shortDescription, String synopsis,
                 String broadcastChannelValue, String _href) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.synopsis = synopsis;

        this.broadcastChannelValue = broadcastChannelValue;
        this._href = _href;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getShortDescription() { return shortDescription; }

    public void setShortDescription(String shortDescription) { this.shortDescription = shortDescription; }

    public String getSynopsis() { return synopsis; }

    public void setSynopsis(String synopsis) { this.synopsis = synopsis; }

    public String getBroadcastChannelValue() { return broadcastChannelValue; }

    public void setBroadcastChannelValue(String broadcastChannelValue) { this.broadcastChannelValue = broadcastChannelValue; }

    public String get_Href() { return _href; }

    public void set_Href(String _href) { this._href = _href; }

    public List<Image> getImages() { return this.images; }


    public class Image {

        String width, height, url, type;

        public Image(){
        }

        public String getWidth() { return this.width; }

        public void setWidth(String width) { this.width = width; }

        public String getHeight() { return this.height; }

        public void setHeight(String height) { this.height = height; }

        public String getUrl() { return this.url; }

        public void setUrl(String url) { this.url = url; }

        public String getType() { return this.type; }

        public void setType(String type) { this.type = type; }

    }

}