package com.unaimasa.baseproject.rest.entities;

import java.util.List;

/**
 * Created by UnaiMasa on 21/05/2016.
 */
public class MovieList {

    public List<Movie> _links;

    public MovieList() {

    }

    public List<Movie> getMovieList() { return _links; }

    public void setMovieList(List<Movie> _links) { this._links = _links; }

}
