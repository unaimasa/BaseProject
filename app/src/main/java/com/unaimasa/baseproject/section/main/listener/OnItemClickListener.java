package com.unaimasa.baseproject.section.main.listener;

import com.unaimasa.baseproject.rest.entities.Movie;

/**
 * Created by UnaiMasa on 22/05/2016.
 */
public interface OnItemClickListener {
    void onItemClick(Movie movie);
}
