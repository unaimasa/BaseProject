package com.unaimasa.baseproject.section.detail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.unaimasa.baseproject.Constants;
import com.unaimasa.baseproject.R;
import com.unaimasa.baseproject.rest.IWebAPI;
import com.unaimasa.baseproject.rest.entities.Movie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by UnaiMasa on 22/05/2016.
 */
public class DetailActivity extends AppCompatActivity implements Callback<Movie> {

    // Href Url to get information
    private String href;

    // Image Loader
    private ImageLoader mImageLoader;
    DisplayImageOptions defaultOptions;

    // UI Elements
    private ImageView mMovieImage;
    private TextView mMovieTitle, mMovieBroadcastChannel, mMovieSynopsis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        if(intent != null){
            href = intent.getStringExtra(Constants.IntentExtras.MOVIE_HREF);
        }

        // Action Bar Home As Up Enable
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get Image Loader Instance
        mImageLoader = ImageLoader.getInstance();

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        mMovieImage = (ImageView) findViewById(R.id.iv_movie);
        mMovieTitle = (TextView) findViewById(R.id.tv_movie_title);
        mMovieBroadcastChannel = (TextView) findViewById(R.id.tv_movie_broadcast_channel);
        mMovieSynopsis = (TextView) findViewById(R.id.tv_movie_synopsis);

        // method Call to Get Movie Info
        getMovieInformation();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setMovieInformation(Movie movie) {

        String url = movie.getImages().get(0).getUrl();
        mImageLoader.displayImage(url, mMovieImage, defaultOptions);

        mMovieTitle.setText(movie.getTitle());
        mMovieBroadcastChannel.setText(movie.getBroadcastChannelValue());
        mMovieSynopsis.setText(movie.getSynopsis());
    }


    // ===== Retrofit ===== //
    public void getMovieInformation(){

        setProgressBarIndeterminateVisibility(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.NetworkConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        IWebAPI iWebAPI = retrofit.create(IWebAPI.class);

        // Get Movie Info URL
        String path = href.replace(Constants.NetworkConfig.API_URL, "");

        Call<Movie> call = iWebAPI.getMovie(path);

        // asynchronous call
        call.enqueue(this);
    }

    // Retrofit Calback Methods
    @Override
    public void onResponse(Call<Movie> call, Response<Movie> response) {
        if(response.isSuccessful()){
            setMovieInformation(response.body());
        }else{
            Toast.makeText(DetailActivity.this, Constants.Errors.GENERIC_ERROR_MESSAGE + response.message(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<Movie> call, Throwable t) {
        Toast.makeText(DetailActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
