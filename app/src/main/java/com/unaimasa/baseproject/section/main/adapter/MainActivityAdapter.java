package com.unaimasa.baseproject.section.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unaimasa.baseproject.R;
import com.unaimasa.baseproject.rest.entities.Movie;
import com.unaimasa.baseproject.section.main.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by UnaiMasa on 21/05/2016.
 */
public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {

    private List<Movie> mDataset;
    private final OnItemClickListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextViewTitle;
        public TextView mTextViewShortDescription;

        public ViewHolder(View view) {
            super(view);
            mTextViewTitle = (TextView) view.findViewById(R.id.tv_title);
            mTextViewShortDescription = (TextView) view.findViewById(R.id.tv_short_description);
        }

        public void bind(final Movie movie, final OnItemClickListener listener) {

            mTextViewTitle.setText(movie.getTitle());
            mTextViewShortDescription.setText(movie.getShortDescription());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(movie);
                }
            });

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MainActivityAdapter(List<Movie> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MainActivityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        // ...
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from the dataset at this position
        Movie movie = mDataset.get(position);
        holder.bind(movie, mListener);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Updates de List of Movies
    public void add(List<Movie> listMovies){
        this.mDataset = removeEmptyMovies(listMovies);
        notifyDataSetChanged();
    }

    public List<Movie> removeEmptyMovies(List<Movie> listMovies){
        List<Movie> finalListMovies = new ArrayList<Movie>();
        for(int i = 0; i < listMovies.size(); i++){
            Movie m = listMovies.get(i);
            if(m.getTitle() != null){
                finalListMovies.add(m);
            }
        }
        return finalListMovies;
    }
}
