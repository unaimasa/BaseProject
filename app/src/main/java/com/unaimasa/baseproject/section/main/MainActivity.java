package com.unaimasa.baseproject.section.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.unaimasa.baseproject.Constants;
import com.unaimasa.baseproject.R;
import com.unaimasa.baseproject.section.detail.DetailActivity;
import com.unaimasa.baseproject.section.main.adapter.MainActivityAdapter;
import com.unaimasa.baseproject.rest.IWebAPI;
import com.unaimasa.baseproject.rest.entities.Info;
import com.unaimasa.baseproject.rest.entities.Movie;
import com.unaimasa.baseproject.section.main.listener.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<Info> {

    // List Elements
    private List<Movie> movieList = new ArrayList<>();

    // Recycler View Elements
    private RecyclerView mRecyclerView;
    private MainActivityAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        mAdapter = new MainActivityAdapter(movieList, new OnItemClickListener() {
            @Override
            public void onItemClick(Movie movie) {
                showMovieInformation(movie);
            }
        });

        mRecyclerView.setAdapter(mAdapter);

        // method Call to Get List of Movies
        getMoviesInformation();
    }

    public void showMovieInformation(Movie movie){
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.IntentExtras.MOVIE_HREF, movie.get_Href());
        startActivity(intent);
    }

    // ===== Retrofit ===== //
    public void getMoviesInformation(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.NetworkConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        IWebAPI iWebAPI = retrofit.create(IWebAPI.class);

        Call<Info> call = iWebAPI.getMovieList();

        // asynchronous call
        call.enqueue(this);
    }

    // Retrofit Calback Methods
    @Override
    public void onResponse(Call<Info> call, Response<Info> response) {
        if(response.isSuccessful()){
            movieList = response.body().get_Links().get(2).getMovieList();
            mAdapter.add(movieList);
        }else{
            Toast.makeText(MainActivity.this, Constants.Errors.GENERIC_ERROR_MESSAGE + response.message(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<Info> call, Throwable t) {
        Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
