package com.unaimasa.baseproject;

/**
 * Created by UnaiMasa on 21/05/2016.
 */
public class Constants {

    public interface NetworkConfig {
        String API_URL = "http://content.ott.sky.com";
    }

    public interface Errors {
        String GENERIC_ERROR_MESSAGE = "An error has ocurred ";
    }

    public interface IntentExtras {
        String MOVIE_HREF = "movie_href_for_detail";
    }

}
